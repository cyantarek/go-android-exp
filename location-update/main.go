package main

import (
	"encoding/json"
	"github.com/labstack/echo"
	"log"
	"net/http"
	"os/exec"
)

type Battery struct {
	Health      string  `json:"health"`
	Percentage  int     `json:"percentage"`
	Status      string  `json:"status"`
	Temperature float64 `json:"temperature"`
}

type Location struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Altitude  float64 `json:"altitude"`
	Accuracy  float64 `json:"accuracy"`
	Bearing   float64 `json:"bearing"`
	Speed     float64 `json:"speed"`
	ElapsedMs int64   `json:"elapsedMs"`
	Provider  string  `json:"provider"`
}

func main() {
	app := echo.New()

	app.GET("/status/battery", func(c echo.Context) error {
		cmd := exec.Command("termux-battery-status")

		//StdoutPipe returns a pipe that will be connected to
		//the command's standard output when the command starts
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			log.Fatal(err)
		}
		if err := cmd.Start(); err != nil {
			log.Fatal(err)
		}

		var batteryStatus Battery
		if err := json.NewDecoder(stdout).Decode(&batteryStatus); err != nil {
			log.Fatal(err)
		}

		//Wait waits for the command to exit
		//It must have been started by Start
		if err := cmd.Wait(); err != nil {
			log.Fatal(err)
		}

		return c.JSON(http.StatusOK, batteryStatus)
	})

	app.GET("/status/location", func(c echo.Context) error {
		cmd := exec.Command("termux-location", "-p", "network")

		//StdoutPipe returns a pipe that will be connected to
		//the command's standard output when the command starts
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			log.Fatal(err)
		}
		if err := cmd.Start(); err != nil {
			log.Fatal(err)
		}

		var locationStatus Location
		if err := json.NewDecoder(stdout).Decode(&locationStatus); err != nil {
			log.Fatal(err)
		}

		//Wait waits for the command to exit
		//It must have been started by Start
		if err := cmd.Wait(); err != nil {
			log.Fatal(err)
		}

		return c.JSON(http.StatusOK, locationStatus)
	})

	app.StdLogger.Fatal(app.Start(":8900"))
}
